import React from 'react';
import './App.css';
import SymptomsForm from './SymptomsForm';
import Map from './Map';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      location: {
        longitude: undefined,
        latitude: undefined
      }
    }
  }

  componentDidMount() {
    this.getPosition();
  }

  getPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(
        success => {
          this.setState({
            location: success.coords
          });
        },
        error => {
          this.setState({
            location: {
              longitude: undefined,
              latitude: undefined
            }
          })
        });
    }
  }

  render() {
    return (
      <div className='App'>
        <header>
          header
      </header>
        <main>
          <SymptomsForm location={this.state.location}/>
          <Map location={this.state.location}/>
        </main>
        <footer>
          footer
      </footer>
      </div>
    );
  }
}

export default App;
