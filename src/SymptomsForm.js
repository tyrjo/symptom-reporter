/* global fetch */
import React from 'react';
import config from './config';
import './SymptomsForm.css';

class SymptomsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            zipcode: '',
            temperature: ''
        }
    }

    onSubmit() {
        return fetch(config.HOST + '/report', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            mode: 'cors',
            body: JSON.stringify({
                user_id: '1',
                temperature: this.state.temperature,
                zipcode: this.state.zipcode
            })
        });
    }

    onZipcodeChange(value) {
        this.setState({
            zipcode: value
        });
    }

    onTempChange(value) {
        this.setState({
            temperature: value
        });
    }

    render() {
        return (
            <div className='SymptomsForm'>
                Location: {this.props.location.longitude || 'Unknonwn'}, {this.props.location.latitude || 'Unknonwn'}
                <form
                    onSubmit={(event) => {
                        event.preventDefault();
                        this.onSubmit(event);
                    }}
                >
                    <ul>
                        <li><label htmlFor='zipcode'>Zipcode:</label>
                            <input
                                type='tel'
                                id='zipcode'
                                name='zipcode'
                                value={this.state.zipcode}
                                onChange={(event) => {
                                    this.onZipcodeChange(event.target.value);
                                }}
                            ></input>
                        </li>
                        <li>
                            <label
                                htmlFor='temperature'>Temperature:</label>
                            <input
                                type='number'
                                id='temperature'
                                name='temperature'
                                value={this.state.temperature}
                                onChange={(event) => {
                                    this.onTempChange(event.target.value);
                                }}
                            ></input>
                        </li>
                    </ul>
                    <button type='submit'>Submit To {config.HOST + '/report'}</button>
                </form>
            </div>)
    }
}

export default SymptomsForm;