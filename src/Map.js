import React from 'react';
import './Map.css';
import * as d3 from 'd3';
import * as topojson from "topojson-client";

class Map extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidUpdate() {
        d3.json(`${process.env.PUBLIC_URL}/data/counties-10m.json`)
            .then((us) => {
                var svg = d3.select('svg')
                var path = d3.geoPath().projection(d3.geoAlbersUsa());
                svg.append('g')
                    .attr('fill', '#000')
                    .attr('stroke', '#000')
                    .selectAll('path')
                    .data(topojson.feature(us, us.objects.counties).features)
                    .enter().append('path')
                    .attr('d', path);

                svg.append('path')
                    .attr('stroke', '#fff')
                    .attr('stroke-width', 1)
                    .attr('d', path(topojson.mesh(us, us.objects.states, (a, b) => a !== b)));
                console.log(this.props.location);
                let {longitude, latitude} = this.props.location;
                var pointData = {
                    "type": "Point",
                    "coordinates": [longitude, latitude]
                }
                svg.append('path')
                    .attr('fill', '#ff0000')
                    .attr('d', path(pointData))
            });
    }


    render() {
        return (
            <section className='Map'>
                <svg>
                </svg>
            </section>
        )
    }
}

export default Map;