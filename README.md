# Symptom Reporter

## Contributing
See [wiki](https://gitlab.com/tyrjo/symptom-reporter/-/wikis/home)

## Goals
* Allow early detection and monitoring of potential Covid-19 cases
* Self-reported
* Anonymized
* Web-based
* No special equipment required
* Able to accept, aggregate and display the daily inputs from 50% of american population
* No account creation required (sign-in with Google, Facebook, etc or purely cookie based)
* Does not need 100% accuraccy
* Minimizes malicious reporting