import json
import boto3
from datetime import datetime

def enable_cors(function):
    def wrapper(*args, **kwargs):
        response = function(*args, **kwargs)
        response['headers'] = {
            'Access-Control-Allow-Origin': '*'
        }
        return response
    return wrapper

def filter_data(data):
    temperature = data.get('temperature')
    zipcode = data.get('zipcode')
    user_id = data.get('user_id')
    if temperature:
        temp = float(temperature)
        if temp < 94:
            temp = (temp * 9 / 5) + 32
            temperature = str(temp)
    
    if not temperature or not zipcode or not user_id:
        return None
    else:
        return {
        'user_id': data.get('user_id', None),
        'temperature': temperature,
        'zipcode': data.get('zipcode', None),
        'timestamp': str(datetime.now())
    }

@enable_cors
def submit_report(event, context):
    
    try:
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('symptom-reporter-dev-report')
        
        if 'body' in event:
            data = filter_data(json.loads(event['body']))
            
            if data:
                table.put_item(Item=data)
                response = {
                    "statusCode": 200,
                    "body": json.dumps(data)
                }
            else:
                response = {
                    "statusCode": 400,
                    "body": "Invalid data"
                }
        else:
            response = {
                "statusCode": 400,
                "body": "No body in POST"
            }
        # Catch invalid response BEFORE returning it to the lambda runtime
        test = json.loads(response['body'])
    except Exception as e:
        response = {
            "statusCode": 400,
            "body": str(e)
        }
    
    return response
