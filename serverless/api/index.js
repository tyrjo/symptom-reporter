// index.js

const serverless = require('serverless-http');
const cors = require('cors')
const express = require('express');
const AWS = require('aws-sdk');
const get = require('lodash.get');

const app = express();

/* apply middleware */
app.use(cors())
app.use(express.json()) // for parsing application/json


app.use(express.static('api/static'));


app.post('/report', function(req, res, next) {
    const dynamodb = new AWS.DynamoDB()
    const now = Date.now();
    return dynamodb.putItem({
        TableName: 'symptom-reporter-dev-report',
        Item: {
            'user_id': {
                S: req.body.user_id
            },
            'temperature': {
                N: req.body.temperature.toString()
            },
            'zipcode': {
                N: req.body.zipcode.toString()
            },
            'timestamp': {
                S: now.toString()
            }
        }
    })
    .promise()
    .then(function(err, data) {
        if (err) {
            res.json(err);
        }
        res.json(now);
    })
    .catch(next);
});

app.post('/agg', function(req, res) {
    res.json({ '/agg': true });
});

module.exports.handler = serverless(app);
