## Setup
* Install serverless and plugins
  * `curl -o- -L https://slss.io/install | bash`
  * `sls plugin install -n serverless-wsgi`

## Deployment
* In AWS KWS service, create a new KMS key and copy the ARN value
* `export AWS_KMS_KEY_ARN=...`
* `sls deploy`

## Teardown
* `sls remove`

## Test
* Get endpoint URL:
  * `sls info`
* `curl -H "Content-Type: application/json" --data @test/test_post.json <URL from sls info>`